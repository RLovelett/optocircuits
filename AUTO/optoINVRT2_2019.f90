! INLCUDE THE ODE INTEGRATOR and JACOBIAN FILES FROM TAPENADE

      !INCLUDE 'ode.f'
      !INCLUDE 'opkdmain.f'
      INCLUDE 'dvode.f'
      SUBROUTINE FUNC(NDIM,U,ICP,PAR,IJAC,F,DFDU,DFDP) 
!     ---------- ---- 

      IMPLICIT NONE
      INTEGER, INTENT(IN) :: NDIM, ICP(*), IJAC
      DOUBLE PRECISION, INTENT(IN) :: U(NDIM), PAR(*)
      DOUBLE PRECISION, INTENT(OUT) :: F(NDIM)
      DOUBLE PRECISION, INTENT(INOUT) :: DFDU(NDIM,NDIM), DFDP(NDIM,*)
      DOUBLE PRECISION :: dam(NDIM,NDIM), dum(NDIM,1)

      DOUBLE PRECISION duty,period

! FORCING PARAMETERS

      duty=PAR(1)
      period=PAR(2)

! NUMERICAL OR ANALYTICAL DERIVATIVES
 
     !IF (IJAC==0) THEN
     !   CALL MAP_INT(U, pulse, length, light1, light2, F, dam, dum)
     !ELSE IF (IJAC==1) THEN
     !   CALL MAP_INT(U, pulse, length, light1, light2, F, DFDU, dum)
     !ELSE IF (IJAC==2) THEN
     !PRINT *, U
     !print *,duty,period
        CALL MAP_INT(U, duty,period, F, DFDU, dum)
     !ENDIF

     !DFDP(1,1)=0.
     !DFDP(2,1)=0.

     !PRINT *, "F", F(1), F(2)
     !PRINT *, "DFDU", DFDU(1,1), DFDU(1,2)
     !PRINT *, "DFDU", DFDU(2,1), DFDU(2,2)
     !PRINT *, "DFDP", DFDP(1,1), DFDP(1,2)
     !PRINT *, "DFDP", DFDP(2,1), DFDP(2,2)
      
      END SUBROUTINE FUNC

! INTEGRATION ROUTINE - RETURNS YF, V, P

     SUBROUTINE MAP_INT(U0, duty, PERIOD, UF, V, P)

     IMPLICIT NONE     

     EXTERNAL :: augfun
     EXTERNAL :: INTJAC_full
     EXTERNAL :: INTJAC_banded
     DOUBLE PRECISION :: U0, UF, DUTY, PERIOD, EYE, V, P, FNC
     DOUBLE PRECISION :: Y, RWORK, T, TOUT, RELERR, ABSERR,RPAR
     INTEGER :: IOPT
     INTEGER :: EQ, I, J, K, LRW, LIW,IPAR
     INTEGER :: NEQN, IFLAG, IWORK, ISTATE, ITOL, ITASK, MF
     DIMENSION :: U0(3), UF(3), EYE(3,3), V(3,3), P(3,1)
     DIMENSION :: Y(12), RWORK(1002), IWORK(52), ABSERR(12), RELERR(12),RPAR(2),IPAR(2)
     
     EQ=SIZE(U0)
     EYE=0.0
     FORALL(J=1:EQ) EYE(J,J)=1.0

     NEQN=SIZE(Y)
     Y(1:3) = U0(1:3)

! Y IS A VECTOR OF N+1+N^2+2*N

     K=EQ+1
     DO J=1,EQ
        DO I=1,EQ
            Y(K)=EYE(I,J)
            K=K+1
        END DO
     END DO
     DO K=EQ+1+EQ**2,NEQN
        Y(K)=0.0
     END DO

     T=0.0
     ITOL = 1
     RELERR=1.D-13
     ABSERR=1.D-13

! INTEGRATE FOR ONE PERIOD
     ! INPUT LOWER AND UPPER HALF BANDWIDTHS IN IWORK 1 and 2
     !IWORK(1) = 38
     !IWORK(2) = 38
     RWORK = 0 ! reinitialize
     IWORK = 0 ! reinitialize
     IWORK(6) = 100000 ! max number of steps
     ISTATE=1
     IOPT = 1
     MF = 21
     LRW = SIZE(RWORK)
     LIW = SIZE(IWORK)
     TOUT = PERIOD
     RPAR(1) = DUTY
     RPAR(2) = PERIOD
     
     ITASK = 4
     RWORK(1) = DUTY*PERIOD! Set that DUTY*PERIOD is a critical value
     if (duty >0)then
     CALL DVODE(augfun, NEQN, Y,T, DUTY*PERIOD, ITOL, RELERR, ABSERR,& 
               & ITASK,ISTATE, IOPT, RWORK, LRW, IWORK, LIW,& 
               & INTJAC_full, MF, RPAR, IPAR)   
     end if
     ITASK = 4
     RWORK(1) = PERIOD
     !MF = 21
     !LRW = SIZE(RWORK)
     !LIW = SIZE(IWORK)
     !ITOL = 1
     !RELERR=1.D-4
     !ABSERR=1.D-4
     !NEQN=SIZE(Y)
     !print *,'first', T,ISTATE
     if (DUTY <1)then
     CALL DVODE(augfun, NEQN, Y,T, TOUT, ITOL, RELERR, ABSERR,& 
               & ITASK,ISTATE, IOPT, RWORK, LRW, IWORK, LIW,& 
               & INTJAC_full, MF, RPAR, IPAR)   
     end if
     !print *,'second', T,ISTATE
! GET YF = Y(T), V(T) and P(T)
     !print *, 'end state', istate

     UF(1:3)=Y(1:3)
     K=EQ+1
     DO J=1,EQ
        DO I=1,EQ
            V(I,J)=Y(K)
            K=K+1
        END DO
     END DO
     K=EQ+1+EQ**2
     J=1!DO J=1
        DO I=1,2
            P(I,J)=Y(K)
            K=K+1
        END DO
     !END DO
     !CALL ODE_SYSTEM(Y(1:EQ),FNC)
     !P(:,2)=P(:,2) - 3.*((2.0*PI)/(OMEGA**2)) * FNC(1:2)

     RETURN
     END

! ACTUAL FUNCTION PASSED TO THE INTEGRATOR
! Y is the "full" system: U + Jac + JacP
! YP is Y', i.e. dY/dt
! NEQN is the TOTAL (N+N**2+NP) number equations
! N_S is the number of state variables
     SUBROUTINE augfun (NEQN, T, Y, YP, RPAR, IPAR)
     IMPLICIT NONE
     DOUBLE PRECISION :: T, Y, YP, DUTY, PERIOD, PI, DF, V, DV, DP, P, DPP
     DOUBLE PRECISION :: RPAR
     INTEGER :: NEQN, I, J, K, IPAR,N_S
     DIMENSION Y(12), YP(12), DF(3,3), V(3,3), DV(3,3)
     DIMENSION RPAR(2)
     N_S=SIZE(DF,1)
     !PI = 4.D0*DATAN(1.D0)
     CALL ODE_SYSTEM(Y(1:N_S),YP(1:N_S),T,RPAR)
     CALL STATE_JAC(Y(1:N_S),DF,T,RPAR)
     !if (t<1e-5)then
     !    print *,'here:',DP
     !end if
     !print *,'new iter'
     !print *, Y
     !print *,'NEW ITER'
     K=N_S+1
     DO J=1,N_S
        DO I=1,N_S
            V(I,J)=Y(K)
            K=K+1
        END DO
     END DO
     !K=N_S+1+N_S**2

! dV/dt = dF/dx*V
! dP/dt = dF/dx*P + dF/dp

     DV=MATMUL(DF,V)

     K=N_S+1
     DO J=1,N_S
        DO I=1,N_S
            YP(K)=DV(I,J)
            K=K+1
        END DO
     END DO
     RETURN
     END

! SYSTEM OF ODEs

     SUBROUTINE ODE_SYSTEM(U,F,T,RPAR)
         implicit none
     DOUBLE PRECISION :: U, F, DUTY, PERIOD, T,RPAR
     DIMENSION :: U(3), F(3), RPAR(2)

DOUBLE PRECISION, external :: pow
DOUBLE PRECISION, external :: hill_active
DOUBLE PRECISION, external :: hill_repress
DOUBLE PRECISION, external :: dhill_activedx
DOUBLE PRECISION, external :: dhill_repressdx
! STATE VARIABLES
DOUBLE PRECISION EL222A
DOUBLE PRECISION GAL80
DOUBLE PRECISION GAL4
DOUBLE PRECISION GFP
DOUBLE PRECISION EL222D ! KNOWN FROM CONSERVATION
! PARAMETERS
DOUBLE PRECISION tau_a
DOUBLE PRECISION tau_GAL80
DOUBLE PRECISION tau_GFP
DOUBLE PRECISION tau_GAL4
DOUBLE PRECISION K_EL222a
DOUBLE PRECISION K_GAL80
DOUBLE PRECISION n_EL222a
DOUBLE PRECISION n_GAL80
DOUBLE PRECISION tau_LAD
DOUBLE PRECISION tau_DD
!BIFURCATION PARAMS (I.E. CONTROLS)
DOUBLE PRECISION light

tau_a     = 30
tau_GAL80 = 16.1*3600
tau_GFP   = 2.1*3600
tau_GAL4  = 2.7*3600
K_EL222a  = 0.059
K_GAL80   = 0.3625
n_EL222a  = 3.6001
n_GAL80   = 7.418
tau_LAD   = 89
tau_dd    = 5*3600*1000

DUTY = RPAR(1)
PERIOD = RPAR(2)

!print *, RPAR

EL222A = U(1)
GAL80 = U(2)
           
GFP = U(3)

!CONSERVATION EQUATIONS
EL222d = 1 - EL222A


!OPTOGENETIC REACTION
if (T<DUTY*PERIOD) then
light = 1.0
else
light = 0.0
end if
if (DUTY>=1) then
    light=1.0
end if
if (DUTY<0)then
    light= 0
end if

F(1) = (light-EL222a)/tau_a
F(2) = (hill_active(EL222a,n_EL222a,K_EL222a)-GAL80)/tau_GAL80
             
F(3) = (hill_repress(GAL80,n_GAL80,K_GAL80) - GFP)/tau_GFP          



!print *, T,F,light
     RETURN
     END

! JACOBIAN OF ODEs

     SUBROUTINE STATE_JAC(U,DF,T,RPAR)
     IMPLICIT NONE
DOUBLE PRECISION, external :: hill_active
DOUBLE PRECISION, external :: hill_repress
DOUBLE PRECISION, external :: dhill_activedx
DOUBLE PRECISION, external :: dhill_repressdx
     DOUBLE PRECISION :: RPAR,U, DF, DUTY, PERIOD
     DOUBLE PRECISION ud, f, fd,T
     INTEGER :: I,J
     DIMENSION :: U(3), DF(3,3), ud(3), f(3), fd(3), RPAR(2)
DOUBLE PRECISION tau_a
DOUBLE PRECISION tau_GAL80
DOUBLE PRECISION tau_GFP
DOUBLE PRECISION tau_GAL4
DOUBLE PRECISION K_EL222a
DOUBLE PRECISION K_GAL80
DOUBLE PRECISION n_EL222a
DOUBLE PRECISION n_GAL80
DOUBLE PRECISION tau_LAD
DOUBLE PRECISION tau_DD
DOUBLE PRECISION EL222A
DOUBLE PRECISION GAL80
DOUBLE PRECISION GAL4
DOUBLE PRECISION GFP
DOUBLE PRECISION EL222D ! KNOWN FROM CONSERVATION
DOUBLE PRECISION LIGHT
tau_a     = 30
tau_GAL80 = 16.1*3600
tau_GFP   = 2.1*3600
tau_GAL4  = 2.7*3600
K_EL222a  = 0.059
K_GAL80   = 0.3625
n_EL222a  = 3.6001
n_GAL80   = 7.418
tau_LAD   = 89
tau_dd    = 5*3600*1000
EL222A = U(1)
GAL80 = U(2)
            
GFP = U(3)
DUTY = RPAR(1)
PERIOD = RPAR(2)
!OPTOGENETIC REACTION
if (T<DUTY*PERIOD) then
light = 1.0
else
light = 0.0
end if
if (DUTY<0)then
    light = 0
end if
if (DUTY>=1)then
    light = 1
end if
     ! Call tap_jac
     DF(1,1) =-1/tau_a
     DF(1,2) =0
               
     DF(1,3) =0
     DF(2,1) =dhill_activedx(EL222a,n_EL222a,K_EL222a)/tau_GAL80
     DF(2,2) =-1/tau_GAL80
                
     DF(2,3) =0
                
                  
                
                  
     DF(3,1) =0
     DF(3,2) =dhill_repressdx(GAL80,n_GAL80,K_GAL80)/tau_GFP
                
     DF(3,3) =-1/tau_GFP
    !print *,DF
     
     
     RETURN
     END

! JACOBIAN OF ODEs FOR PARAMETERS


SUBROUTINE STPNT(NDIM,U,PAR,T)
!     ---------- -----

IMPLICIT NONE
INTEGER, INTENT(IN) :: NDIM
DOUBLE PRECISION, INTENT(INOUT) :: U(NDIM),PAR(*)
DOUBLE PRECISION, INTENT(IN) :: T

DOUBLE PRECISION duty, period

       duty  =  0.00
       period= 10.0
       PAR(1) = duty
       PAR(2) = period
       
U(1) = 0
U(2) = 0                
U(3) = 1.               
                         

      END SUBROUTINE STPNT

      SUBROUTINE BCND
      END SUBROUTINE BCND

      SUBROUTINE ICND
      END SUBROUTINE ICND

      SUBROUTINE FOPT
      END SUBROUTINE FOPT

      SUBROUTINE PVLS(NDIM,U,PAR)
!     ------------------------------

      IMPLICIT NONE
      EXTERNAL FUN_STATE_SPACE
      EXTERNAL INTJAC_STATE_SPACE
      INTEGER, INTENT(IN) :: NDIM
      DOUBLE PRECISION, INTENT(IN) :: U(NDIM)
      DOUBLE PRECISION, INTENT(INOUT) :: PAR(*)

      DOUBLE PRECISION, EXTERNAL :: GETP,GETU2
      INTEGER NDX,NCOL,NTST,I
      INTEGER NEQN, ITOL, ITASK, ISTATE,IOPT,LRW,IWORK,LIW, MF, IPAR
      DOUBLE PRECISION  Y,T,TOUT,RELERR,ABSERR,RWORK,RPAR,DT,IBA_T1, IBA_T2
      DOUBLE PRECISION duty, period,IBA_M1, IBA_M2
      DIMENSION RWORK(90),IWORK(34), IBA_T1(100),RPAR(2),IBA_T2(100)
      DIMENSION Y(NDIM), RELERR(NDIM),ABSERR(NDIM),IPAR(2)
!---------------------------------------------------------------------- 
! NOTE : 
! Parameters set in this subroutine should be considered as ``solution 
! measures'' and be used for output purposes only.
! 
! They should never be used as `true'' continuation parameters. 
!
! They may, however, be added as ``over-specified parameters'' in the 
! parameter list associated with the AUTO-Constant NICP, in order to 
! print their values on the screen and in the ``p.xxx file.
!
! They may also appear in the list associated with AUTO-constant NUZR.
!
!---------------------------------------------------------------------- 
! For algebraic problems the argument U is, as usual, the state vector.
! For differential equations the argument U represents the approximate 
! solution on the entire interval [0,1]. In this case its values can
! be accessed indirectly by calls to GETP, as illustrated below, or
! by obtaining NDIM, NCOL, NTST via GETP and then dimensioning U as
! U(NDIM,0:NCOL*NTST) in a seperate subroutine that is called by PVLS.
!---------------------------------------------------------------------- 
! INTEGRATE FOR ONE PERIOD
     ! INPUT LOWER AND UPPER HALF BANDWIDTHS IN IWORK 1 and 2
     !IWORK(1) = 38
     !IWORK(2) = 38
!     NEQN = 4
!     ABSERR=1.d-8
!     RELERR=1.d-8
!     RWORK = 0 ! reinitialize
!     IWORK = 0 ! reinitialize
!     IWORK(6) = 100000 ! max number of steps
!     ISTATE=1
!     IOPT = 1
!     MF = 21
!     LRW = SIZE(RWORK)
!     LIW = SIZE(IWORK)
!     DUTY = PAR(1)
!     PERIOD = PAR(2)
!     TOUT = PERIOD
!     RPAR(1) = DUTY
!     RPAR(2) = PERIOD
!     ITOL = 1
!     ITASK = 4
!     RWORK(1) = DUTY*PERIOD! Set that DUTY*PERIOD is a critical value
!     DT = DUTY*PERIOD/100.
!     Y = U
!     I = 1
!     T = 0 !reinitialize
!     DO I=1,100
!       IF (I<100)then
!         CALL DVODE(FUN_STATE_SPACE, NEQN, Y,T, T+DT, ITOL, &
!                 & RELERR, ABSERR,& 
!                 & ITASK,ISTATE, IOPT, RWORK, LRW, IWORK, LIW,& 
!                 & INTJAC_STATE_SPACE, MF, RPAR, IPAR)
!       ELSE
!         CALL DVODE(FUN_STATE_SPACE, NEQN, Y,T, DUTY*PERIOD, ITOL, &
!                 & RELERR, ABSERR,& 
!                 & ITASK,ISTATE, IOPT, RWORK, LRW, IWORK, LIW,& 
!                 & INTJAC_STATE_SPACE, MF, RPAR, IPAR)
!       END IF
!       IBA_T1(I) = Y(4)
!     ENDDO
!     IBA_M1 = sum(IBA_T1)/size(IBA_T1)
!     ITASK = 4
!     RWORK(1) = PERIOD
!     DT = (PERIOD-DUTY*PERIOD)/100.
!     DO I=1,100
!       IF (I<100) then
!         CALL DVODE(FUN_STATE_SPACE, NEQN, Y,T, T+DT, ITOL, &
!                 & RELERR, ABSERR,& 
!                 & ITASK,ISTATE, IOPT, RWORK, LRW, IWORK, LIW,& 
!                 & INTJAC_STATE_SPACE, MF, RPAR, IPAR)
!       ELSE
!         CALL DVODE(FUN_STATE_SPACE, NEQN, Y,T, PERIOD, ITOL, &
!                 & RELERR, ABSERR,& 
!                 & ITASK,ISTATE, IOPT, RWORK, LRW, IWORK, LIW,& 
!                 & INTJAC_STATE_SPACE, MF, RPAR, IPAR)
!       END IF
!       IBA_T2(I) = Y(4)
!     ENDDO
!     IBA_M2 = sum(IBA_T2)/size(IBA_T2)
!      
!     PAR(6)=(DUTY*PERIOD*IBA_M1 + (PERIOD-DUTY*PERIOD)*IBA_M2)/PERIOD! PUT FINAL RESULT HERE       
!---------------------------------------------------------------------- 
! The first argument of GETP may be one of the following:
!        'NRM' (L2-norm),     'MAX' (maximum),
!        'INT' (integral),    'BV0 (left boundary value),
!        'MIN' (minimum),     'BV1' (right boundary value).
!        'MNT' (t value for minimum)
!        'MXT' (t value for maximum)
!        'NDIM', 'NDX' (effective (active) number of dimensions)
!        'NTST' (NTST from constant file)
!        'NCOL' (NCOL from constant file)
!        'NBC'  (active NBC)
!        'NINT' (active NINT)
!        'DTM'  (delta t for all t values, I=1...NTST)
!        'WINT' (integration weights used for interpolation, I=0...NCOL)
!
! Also available are
!   'STP' (Pseudo-arclength step size used).
!   'FLD' (`Fold function', which vanishes at folds).
!   'BIF' (`Bifurcation function', which vanishes at singular points).
!   'HBF' (`Hopf function'; which vanishes at Hopf points).
!   'SPB' ( Function which vanishes at secondary periodic bifurcations).
!   'EIG' ( Eigenvalues/multipliers, I=1...2*NDIM, alternates real/imag parts).
!   'STA' ( Number of stable eigenvalues/multipliers).
!---------------------------------------------------------------------- 
      END SUBROUTINE PVLS


double precision function pow(i,j)
  implicit none
  double precision i,j
  pow = i**j
end function pow

double precision function hill_active(x,n,K)
  implicit none
  double precision x,n,K
  if (x>0)then
      hill_active = x**n/(K**n + x**n)
  else
      hill_active = 0
  end if
end function hill_active

double precision function hill_repress(x,n,K)
  implicit none
  double precision x,n,K
  if (x<0) then
      x = 0
  end if

  hill_repress = K**n/(K**n+x**n)

end function hill_repress

double precision function dhill_activedx(x,n,K)
  implicit none
  double precision x,n,K
  if (x>0) then
      dhill_activedx = ((K**n + x**n)*n*x**(n-1)&
                       &- n*x**(2*n-1))/(K**n + x**n)**2
  else
      dhill_activedx = 0
  end if

end function dhill_activedx

double precision function dhill_repressdx(x,n,K)
  implicit none
  double precision x,n,K
  if (x<0) then
      x = 0
  end if
  dhill_repressdx = -K**n*n*x**(n-1)/(K**n+x**n)**2
end function dhill_repressdx


SUBROUTINE INTJAC_full (NEQN, T, Y, ML, MU, PD, NROWPD,RPAR, IPAR)
    IMPLICIT NONE
    INTEGER :: NEQN, ML, MU, NROWPD, IPAR,nblocks,nstates
    INTEGER :: K,I,J,IFULL,JFULL,IBAND, JBAND
    DOUBLE PRECISION :: T, Y, PD, DF, RPAR
    DIMENSION DF(3,3), RPAR(2), PD(12,12), Y(12)


    CALL STATE_JAC(Y(1:3),DF,T,RPAR)

    nblocks = 1+3
    nstates = 3
    DO K = 1,nblocks
      DO I = 1+nstates*(K-1),nstates*K
        DO J = 1+nstates*(K-1),nstates*K
          IFULL = I
          JFULL = J
          !IBAND = IFULL-JFULL+MU+1
          !JBAND = JFULL
          PD(IFULL,JFULL) = DF(mod(IFULL-1,3)+1,mod(JFULL-1,3)+1)
        END DO
      END DO
    END DO
    !print *,PD

END SUBROUTINE INTJAC_full

SUBROUTINE INTJAC_STATE_SPACE (NEQN,T,Y,ML,MU,PD,NROWPD,RPAR,IPAR)
    IMPLICIT NONE
    INTEGER :: NEQN, ML, MU, NROWPD, IPAR
    DOUBLE PRECISION :: T,Y,PD,DF, RPAR
    DIMENSION Y(3), RPAR(2), PD(3,3), DF(3,3)

    CALL STATE_JAC(Y,DF,T,RPAR)

    PD = DF

END SUBROUTINE INTJAC_STATE_SPACE
     
SUBROUTINE FUN_STATE_SPACE (NEQN, T, Y, YP, RPAR, IPAR)
    IMPLICIT NONE
    INTEGER :: NEQN, IPAR
    DOUBLE PRECISION :: T,Y,YP, RPAR
    DIMENSION Y(3), YP(3), RPAR(2)
    
    CALL ODE_SYSTEM(Y,YP,T,RPAR)

END SUBROUTINE FUN_STATE_SPACE
