# OptoCircuits

This repository contains the companion code for the manuscript "Dynamical modeling of optogenetic circuits in yeast for metabolic engineering applications," in ACS Synthetic Biology (doi: [10.1021/acssynbio.0c00372](http://dx.doi.org/10.1021/acssynbio.0c00372))

![Circuit Schematic](./schematic.svg)

# Dependencies
- [NumPy](https://numpy.org/) (tested with 1.18.1)
- [SciPy](https://www.scipy.org/) (tested with 1.4.1)

The demo notebooks also require [Jupyter notebooks](https://jupyter.org/), [Seaborn](https://seaborn.pydata.org/), and [Matplotlib](https://matplotlib.org/).

# Installation

To install, enter:
    
    python setup.py install

# Demonstrations
The demonstrations contain examples of computing dose-response curves and controlling expression using a simple implementation of model predictive control.

# AUTO
To quickly compute many dose response curves, the most efficient tool available is [AUTO](http://indy.cs.concordia.ca/auto/).
AUTO is a collection of highly optimized FORTRAN functions designed for numerical continuation of nonlinear differential equations that is widely used by the dynamical systems community.
The AUTO directory contains function and constant files required to compute several dose response curves using many forcing periods, to generate "expression surfaces," as described in the manuscript.
