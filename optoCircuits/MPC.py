'''
A class for model predictive control of optogenetic systems using on/off duty cycles
'''
import numpy as np
import scipy.optimize as spo
import scipy.integrate as spi


class MPC:

    def __init__(self,
                 model,
                 model_jac,
                 theta_model,
                 theta_plant,
                 x0,
                 x0hat,
                 u0,
                 ref0,
                 GFP_index,
                 forcing_period = 80,
                 delta_t = 800,
                 horizon = 6,
                 u_future0 = np.array([0.05, 0.05,0.05,0.05,0.05,0.05]),
                 InnovGain= 0.1):
        '''
        model is the odes that define the circuit: f(t,x,u)
        model_jac is the jacobian: g(t,x,u)
        theta_model is the set of parameters that MPC will you
        theta_plant is the "true" set of parameters
        x0 is the initial state of the model
        u0 is the input for t = 0
        forcing period is the period in seconds
        delta_t is the time step in seconds: must be integer multiple of forcing_period
        horizon is the number of time steps that we optimize over
        InnovGain is the innovation function gain
        '''
        self.x = x0 # current state vector
        self.xhat = x0hat # current estimate
        self.t_step = 0 # Time in number time steps
        self.u = u0 # current input
        self.ref = ref0
        self.u_future0 = u_future0 #initial guess for future inputs
        self.forcing_period = forcing_period
        self.delta_t = delta_t
        self.horizon = horizon
        self.theta_model = theta_model
        self.theta_plant = theta_plant
        self.model = model
        self.model_jac = model_jac
        self.GFP_index = GFP_index
        self.GFP_history = np.array([x0[GFP_index]])
        self.u_history = np.array([u0])
        self.ref_history = np.array([ref0])
        self.t_history = np.array([0])
        self.InnovGain=InnovGain

    def objective(self, u_future):
        GFP_future = np.zeros(self.horizon)
        u_series = u_future
        x_sim = self.xhat
        for i in range(self.horizon):
            x_sim = self.integrate_step(x_sim,u_series[i], self.theta_model)
            GFP_future[i] = x_sim[self.GFP_index]
        obj = sum((self.ref[1::] - GFP_future[1::])**2)
        return obj
    def optimize_u(self):

        f = lambda u_future : self.objective(u_future)

        res = spo.minimize(f, self.u_future0,
                           method = 'L-BFGS-B',
                           bounds = (((0., 1.),)*len(self.u_future0)),
                          )#options = {'xtol' : 1e-9} )
        u_future = res.x

        return u_future

    def integrate_step(self,x0,u_step,theta):
        def ufun(t):
            if u_step <= 0:
                return np.array([0])
            elif u_step >= 1:
                return np.array([1])
            elif np.mod(t,self.forcing_period)<u_step*self.forcing_period:
                return np.array([1])
            else:
                return np.array([0])
        switch = u_step*self.forcing_period
        if switch > 0 and switch < 1:
            stops1 = np.linspace(switch, self.delta_t-switch, int(self.delta_t/self.forcing_period))
            stops2 = np.linspace(self.forcing_period,self.delta_t, int(self.delta_t/self.forcing_period))
            stops = np.sort(np.append(stops1,stops2))
        else:
            stops = np.linspace(self.forcing_period/2,self.delta_t,int(self.delta_t/self.forcing_period*2))

        for i in range(len(stops)):
            if i == 0:
                y, infodict = spi.odeint(lambda x,t : self.model(t,x,ufun,theta),
                                         x0,
                                         [0,stops[i]],
                                         Dfun = lambda x,t : self.model_jac(t,x,ufun,theta),
                                         full_output=True,
                                         rtol = 1e-13,atol = 1e-13,
                                         mxstep = int(1e9),
                                         tcrit = stops)
            else:
                x0 = y[-1]
                y, infodict = spi.odeint(lambda x,t : self.model(t,x,ufun,theta),
                                         x0,
                                         [stops[i-1],stops[i]],
                                         Dfun = lambda x,t : self.model_jac(t,x,ufun,theta),
                                         full_output=True,
                                         rtol = 1e-13,atol = 1e-13,
                                         mxstep =int(1e9),
                                         tcrit = stops[i::])
        #pdb.set_trace()
        return y[1]


    def time_step(self,next_ref):
        # Find the best u(t+1:t+h) based on the model
        u_future = self.optimize_u()
        self.u = u_future[0]
        # "Measure" x(t+1) with by integrating with the "true" parameters
        x_measure = self.integrate_step(self.x,self.u,self.theta_plant)
        # Prepare for next time step
        self.x = x_measure
        self.xhat = x_measure + self.InnovGain * (x_measure[self.GFP_index] - self.xhat[self.GFP_index])
        self.xhat[self.xhat < 0] = 0
        self.xhat[self.xhat > 1] = 1
        self.u_future0 = np.append(u_future[1::],u_future[-1])
        # gradient estimation gets flat at u=1, so change it.
        for i in range(len(self.u_future0)):
            if self.u_future0[i] == 1:
                self.u_future0[i] = 0.98
        self.ref = np.append(self.ref[1::],np.array([next_ref]))
        self.t_step = self.t_step+1
        self.GFP_history = np.append(self.GFP_history,self.x[self.GFP_index])
        self.u_history = np.append(self.u_history,np.array([self.u]))
        self.ref_history = np.append(self.ref_history,np.array([next_ref]))
