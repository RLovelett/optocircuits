'''
This file defines the circuitClass, which is the parent class for each of the individual
optogenetic circuit models.
'''
import numpy as np
import scipy.optimize as _spo
import optoCircuits.forced_limit_cycle_solver as _flcs

class circuitClass():

    def __init__():
        pass

    def getSteadyStateON(self, theta=np.array([-1])):
        '''
        Get the steady state values of each species when the light is ON.

        Parameters
        ---------
        theta    : parameters for the circuit (default -1 uses self.theta)
        Returns
        --------
        Numpy array containing the steady state values
        '''

        x0= np.ones(self.nStates)*0.5
        ufun = lambda t : [1]
        xON= _spo.fsolve(lambda x : self.model(0,x,ufun,theta=theta), x0,
                         fprime= lambda x: self.jacobian(0,x,ufun,theta=theta))

        return xON

    def getSteadyStateOFF(self, theta=np.array([-1])):
        '''
        Get the steady state values of each species when the light is OFF.

        Parameters
        ---------
        theta    : parameters for the circuit (default -1 uses self.theta)
        Returns
        --------
        Numpy array containing the steady state values
        '''

        x0= np.ones(self.nStates)*0.5
        ufun = lambda t : [0]
        xOFF= _spo.fsolve(lambda x : self.model(0,x,ufun,theta=theta), x0,
                         fprime= lambda x: self.jacobian(0,x,ufun,theta=theta))

        return xOFF



    def getDoseResponse(self,T, doses = np.linspace(0,1,100)):
        '''
        Get the dose response curve of the optogenetic circuit model.

        Parameters
        ---------
        T      : Forcing period {seconds}
        doses  : doses at which to calculate GFP level
        kwargs : additional keyword arguments to pass to the limit cycle solver
        Returns
        --------
        Numpy array containing the initial conditions for the limit cycle at each dose
        '''

        def forcing(t,dose):
            if (t < dose*T):
                return np.array([1])
            else:
                return np.array([0])
        x_lc= np.zeros((len(doses),self.nStates))
        x0= np.ones(self.nStates)*.005
        for i in range(len(doses)):
            x0 = self.getSteadyStateOFF() if i==0 else x_lc[i-1,:]
            events = [] if (doses[i]==0 or doses[i]==1) else [T*doses[i]]
            res = _flcs.Solver(x0,
                               self.model,
                               lambda t : forcing(t,doses[i]),
                               T,
                               self.jacobian,
                               self.jacobian_u,
                               events=events)
            if res[1]>1E-6:
                print("WARNING: Poor convergence.")
            x_lc[i,:]= res[0]
        return x_lc

