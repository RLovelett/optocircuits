

import numpy as np
import scipy.integrate as spi
import pdb
import scipy.sparse as spsp

def Solver(x0,ode,u,T,jacfun, jacfuncontrol, tol_x = 1e-10, tol_fun = 1e-10, MaxIter = 1000, hmax = 0.0, ode_reltol=1e-12,disp = False,disp_res = False, events = []): #,vareqhadle,parameqhandle,ode_opts,ode_int):
    '''
    x0          - Initial guess for x (Presumably off of, but ideally close to, a limit cycle)
    u           - inputs as a function of t, with t from 0 to T; must return array (even if length 1)
    T           - Period
    ode         - ode function, has arguments (t,x,u)
    jacfun      - function for jacobian of ode, has arguments (t,x,u)
    jacfunu     - function for jacobian of params, has arguments (t,x,u)
    tol         - max error
    MaxIter     - Maximum number of iterations
    hmax        - maximum step size for ODE solver (possibly necessary for discontinous control functions)
    ode_reltol  - relative tolerance for the ode solver
    disp        - print error on each iteration (bool)
    events      - a list of times when the input is discontinuous (e.g. pulse ends) for faster integration
    returns:
    x0          - appropriate x0 that sits on a limit cycle
    err_x       - Newton-Raphson error
    '''

    n=len(x0)
    nu=len(u(0))
    x1=x0
    niter=0
    err_x=1+tol_x
    err_fun = 1+tol_fun
    try:
        stops = events + [T]
    except:
        stops = [events] + [T]
    while ((err_x>tol_x or err_fun > tol_fun) and niter < MaxIter):
        V0=np.eye(n)
        V0=np.reshape(V0,n**2,order='F')

        P0=np.zeros(n*nu)

        s0=np.concatenate([x0,V0,P0])
        fun = lambda t,s : Augmented_ODE(t,s,u,n,nu,ode,jacfun,jacfuncontrol)
        intjac = lambda t,s :  Augmented_jacobian(t,s,u,n,nu,ode,jacfun,jacfuncontrol)
        integrator = spi.ode(fun,jac = intjac)
        integrator.set_integrator('vode',method='BDF', rtol = ode_reltol, max_step = hmax,nsteps = 1e4)
        integrator.set_initial_value(s0)
        for stop in stops:
            res = integrator.integrate(stop)
            if not integrator.successful():
                raise Exception('Integration failed at t = {0}. Cannot proceed.'.format(integrator.t))
        x = res[0:n]
        V = res[n:n+n**2]
        P = res[n+n**2::]
        V = np.reshape(V,(n,n),order='F')
        P = np.reshape(P,(n,nu),order='F')

        J  = N_R_Jacobian(x,V,n)
        b  = -(x0-x)
        dx = np.linalg.solve(J,b)
        x1=dx+x0

        err_x=np.linalg.norm(dx)
        err_fun = np.linalg.norm(b)
        niter=niter+1

        x0=x1
        if disp:
            print('err_x = ', err_x)
            print('err_fun = ',err_fun)
    if err_x > tol_x:
        print('MaxIter exceeded and Newton Raphson error still greater than tolerance')
    if err_fun > tol_fun:
        print('MaxIter exceeded and function error still greater than tolerance')
    if disp_res:
        print('x: ', x)
        print('x0 sensitivity: ', V)
        print('Param sensitivity: ', P)
    return x0, err_x


def N_R_Jacobian(x,V,n):

    return np.eye(n)-V

def Augmented_ODE(t,s,u,n,nu,ode,jacfun,jacfuncontrol):
    ds=np.zeros(n+n**2+n*nu)

    Jac  = jacfun(t,s[0:n],u)
    JacP = jacfuncontrol(t,s[0:n],u)
    ds[0:n]      = ode(t,s[0:n],u)
    ds[n:n+n**2] = Var_Eq(s[n:n+n**2],Jac,n)
    ds[n+n**2::] = Param_Eq(s[n+n**2::],Jac,JacP,n,nu)
    return ds

def Augmented_jacobian(t,s,u,n,nu,ode,jacfun,jacfuncontrol):
    x = s[0:n]
    nblocks = 1+n+nu
    block = jacfun(t,x,u)
    integrator_jac = spsp.block_diag([block]*nblocks).toarray()

    return integrator_jac

def Var_Eq(V,J,n):

    dV=np.zeros((n,n))

    V=np.reshape(V,(n,n),order='F')
    dV=np.dot(J,V)

    dV=np.reshape(dV,(n**2),order='F')
    return dV

def Param_Eq(P,J,JP,n,nu):


    dP=np.zeros((n,nu));
    P=np.reshape(P,(n,nu),order='F');

    dP=np.dot(J,P)+JP;

    return np.reshape(dP,(n*nu),order='F')

##################### TEST FUNCTIONS ################################
def testfunlinear(t,x,u):
    A = np.array([[0,1],[-5,-3]])
    B = np.array([[0],[1]])

    return np.dot(A,x) + np.dot(B,u(t))

def testulinear(t):
    if np.mod(t,10)<9:
        return [10000]
    else:
        return [0]

def testjaclinear(t,x,u):
    return np.array([[0,1],[-5,-3]])

def testjacparamlinear(t,x,u):
    return np.array([[0],[1]])



