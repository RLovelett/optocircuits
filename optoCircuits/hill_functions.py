# convenience functions
def hill_active(x, n, K):
    if x >0:
        return x**n/(K**n + x**n)
    else:
        return 0
def hill_repress(x,n,K):
    if x < 0:
        x = 0

    return K**n/(K**n+x**n)

def dhill_activedx(x,n,K):
    if x > 0:
        return ((K**n + x**n)*n*x**(n-1) - n*x**(2*n-1))/(K**n + x**n)**2
    else:
        return 0
def dhill_repressdx(x,n,K):
    if x < 0:
        x = 0
    return -K**n*n*x**(n-1)/(K**n+x**n)**2
