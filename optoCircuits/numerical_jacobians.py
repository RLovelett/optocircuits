'''
Contains a pair of functions to give central limit approximations of
numerical jacobians for use with the forced limit cycle solver
'''
import numpy as np
import pdb

def numJac(ode, t, x, u, h=.1):
    '''
    ode    - the same ode function sent to Solver (arguments: t,x,u)
    t      - time
    x      - state variables
    u      - inputs (control) variables
    h      - stepsize
    returns:
    Jac    - nxn matrix of partial derivatives (dF_i/dx_j, if dx/dt = F(x,u))

    '''
    n = len(x)
    Jac = np.zeros((n,n))
    for i in range(n):
        for j in range(n):
            #h = x[j]*hm
            # Bring x up a half step
            x[j] = x[j] + 0.5*h
            #pdb.set_trace()
            F_plus = ode(t,x,u)[i]
            # Bring x down a whole step
            x[j] = x[j] - h
            F_minus = ode(t,x,u)[i]
            # Bring x back to central point
            x[j] = x[j] + 0.5*h
            Jac[i,j] = (F_plus-F_minus)/h

    return Jac

def numJacControl(ode, t, x, u, h=.1):
    '''
    ode    - the same ode function sent to Solver (arguments: t,x,u)
    t      - time
    x      - state variables
    u      - inputs (control) variables (as function of t)
    h      - stepsize
    returns:
    Jac    - nxnu matrix of partial derivatives (dF_i/du_j, if dx/dt = F(x,u))

    '''
    n  = len(x)
    nu = len(u(t))
    Jac = np.zeros((n,nu))
    for i in range(n):
        for j in range(nu):
            #h = u(t)[j]*hm
            # Bring u up a half step
            def uplus(t,idx):
                ret = u(t)
                ret[j] = ret[j] + .5*h
                return ret
            F_plus = ode(t,x,lambda t : uplus(t,j))[i]
            # Bring u down a whole step
            def uminus(t,idx):
                ret = u(t)
                ret[j] = ret[j] + .5*h
                return u(t)-0.5*h
            F_minus = ode(t,x,lambda t : uminus(t,j))[i]
            Jac[i,j] = (F_plus-F_minus)/h

    return Jac
