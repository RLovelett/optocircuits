'''
    This file contains a class the defines the OptoEXP mechanistic model

    OptoEXP is a an optogenetic circuit that functions by light activation of EL222,
    which in turn activates GFP (or anotherher target gene).

    The constructor takes a single argument: theta, and numpy array containing the
    following parameters, with default values fit to data:

    tau_a       = 30.0   {seconds}
    tau_gfp     = 7560.0 {seconds}
    K_el222a    = 0.056
    n_el222a    = 3.6
'''

import numpy as np
import optoCircuits.hill_functions as _h
from optoCircuits.circuitClass import circuitClass

class optoEXP(circuitClass):

    def __init__(self, theta = np.array([30.0,7560.0,0.056,3.6])):
        self.theta = theta
        self.nStates=2

    def model(self,t,x,u,theta=np.ones(4)*-1):

        EL222a = x[0]
        EL222  = 1 - EL222a
        GFP    = x[1]
        light = u(t)[0]

        if (theta[0]==-1):
            theta= self.theta

        tau_a    = theta[0]
        tau_GFP  = theta[1]
        K_EL222a = theta[2]
        n_EL222a = theta[3]

        dx = np.array([light*EL222/tau_a - (1-light)*EL222a/tau_a,
                       (_h.hill_active(EL222a, n_EL222a, K_EL222a) - GFP)/tau_GFP])

        return dx

    def jacobian(self,t,x,u,theta=np.ones(4)*-1):

        EL222a = x[0]
        EL222  = 1 - EL222a
        GFP    = x[1]

        light = u(t)[0]

        if (theta[0]==-1):
            theta= self.theta

        tau_a    = theta[0]
        tau_GFP  = theta[1]
        K_EL222a = theta[2]
        n_EL222a = theta[3]

        return np.array([[-1/tau_a          , 0   ],
                         [_h.dhill_activedx(EL222a,n_EL222a,K_EL222a)/tau_GFP,  -1/tau_GFP ]])

    def jacobian_u(self,t,x,u,theta=np.ones(4)*-1):

        EL222a = x[0]
        EL222  = 1 - EL222a
        GFP    = x[1]

        light = u(t)[0]

        if (theta[0]==-1):
            theta= self.theta

        tau_a    = theta[0]
        tau_GFP  = theta[1]
        K_EL222a = theta[2]
        n_EL222a = theta[3]

        return np.array([[1/tau_a      ],
                         [0            ]])

