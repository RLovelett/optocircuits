'''
    This file contains a class the defines the OptoINVRT2 mechanistic model:

    OptoINVRT2 is a an optogenetic circuit that functions by light activation of EL222,
    which in turn activates Gal80p expression, which then represses expression of GFP
    (or another target gene). It is differentiated from OptoINVRT1 in that it uses a
    different promoter for Gal4p (the activator for GFP), resulting only in differet parameters.

    The constructor takes a single argument: theta, and numpy array containing the
    following parameters, with default values fit to data:

    tau_a       = 30.0  {seconds}
    tau_gal80p  = 57960 {seconds}
    tau_gfp     = 7560  {seconds}
    K_el222a    = 0.056
    K_gal80p    = 0.76
    n_el222a    = 3.6
    n_gal80p    = 7.39
'''

import numpy as np
import optoCircuits.hill_functions as _h
from optoCircuits.circuitClass import circuitClass

class optoINVRT2(circuitClass):

    def __init__(self, theta = np.array([30.0,57960.0,7560.0,
                                         0.056,0.76,3.6,7.39])):
        self.theta = theta
        self.nStates=3

    def model(self, t,x,u, theta=np.array([-1])):

        EL222a = x[0]
        EL222  = 1 - EL222a
        GAL80  = x[1]
        GFP    = x[2]
        light = u(t)[0]
        if theta[0] == -1:
            theta = self.theta

        tau_a    = theta[0]
        tau_GAL80= theta[1]
        tau_GFP  = theta[2]
        K_EL222a = theta[3]
        K_GAL80  = theta[4]
        n_EL222a = theta[5]
        n_GAL80  = theta[6]

        dx = np.array([light*EL222/tau_a - (1-light)*EL222a/tau_a,
                       (_h.hill_active(EL222a, n_EL222a, K_EL222a) - GAL80)/tau_GAL80,
                       (_h.hill_repress(GAL80,n_GAL80,K_GAL80) - GFP)/tau_GFP])

        return dx

    # Jacobian w.r.t. x for optoINVRT7:
    def jacobian(self,t,x,u, theta= np.ones(1)*-1):

        EL222a = x[0]
        EL222  = 1 - EL222a
        GAL80  = x[1]
        GFP    = x[2]

        light = u(t)[0]

        if theta[0] == -1:
            theta = self.theta

        tau_a    = theta[0]
        tau_GAL80= theta[1]
        tau_GFP  = theta[2]
        K_EL222a = theta[3]
        K_GAL80  = theta[4]
        n_EL222a = theta[5]
        n_GAL80  = theta[6]

        return np.array([[-1/tau_a,0,0],
                         [_h.dhill_activedx(EL222a,n_EL222a,K_EL222a)/tau_GAL80,-1/tau_GAL80,0],
                         [0 , _h.dhill_repressdx(GAL80,n_GAL80,K_GAL80)/tau_GFP,-1/tau_GFP]])

    # Jacobian w.r.t. x for optoINVRT7:
    def jacobian_u(self,t,x,u, theta= np.array([-1])):

        EL222a = x[0]
        EL222  = 1 - EL222a
        GAL80  = x[1]
        GFP    = x[2]

        light = u(t)[0]

        if theta[0]==-1:
            theta= self.theta

        tau_a    = theta[0]
        tau_GAL80= theta[1]
        tau_GFP  = theta[2]
        K_EL222a = theta[3]
        K_GAL80  = theta[4]
        n_EL222a = theta[5]
        n_GAL80  = theta[6]

        return np.array([[1/tau_a      ],
                         [0            ],
                         [0            ]])

