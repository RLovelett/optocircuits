from setuptools import setup, find_packages

setup(name='optoCircuits',
      version='1.0.0',
      url='https://github.com/mypackage.git',
      author='Robert J. Lovelett',
      author_email='robert.lovelett@gmail.com',
      description='Mechanistic models for yeast optogenetic circuits',
      packages=find_packages(),
      install_requires=['numpy >= 1.18.1',
                        'scipy >= 1.4.1'])
